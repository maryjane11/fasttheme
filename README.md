# FastTheme for Phosh

Nice looking theme for Phosh

![Screenshot](screenshot.png)


## How to install?

`cd ~ && mkdir -p .themes && cd .themes && git clone https://gitlab.com/Alaraajavamma/fasttheme && cd fasttheme && ./install.sh `

And reboot and it should just work

Uninstall

`mv ${HOME}/.config/gtk-3.0/backupcss ${HOME}/.config/gtk-3.0/gtk.css && rm -rf ${HOME}/.themes/fasttheme `

And reboot and it should just work

## License
Feel free to do what ever you want with this but no guarantees - this will probably explode your phone xD

## Something else?
If you wan't to help or find issue feel free to contact

Wallpapers are slightly modified but originally from FoxyRiot - thanks. 
