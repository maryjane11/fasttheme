#!/bin/bash
gsettings set org.gnome.desktop.background picture-uri "file:///home/$USER/.themes/fasttheme/tuxgnu.png"
mkdir -p ${HOME}/.config/gtk-3.0/

mv ${HOME}/.config/gtk-3.0/gtk.css ${HOME}/.config/gtk-3.0/backupcss

cd ${HOME}/.config/gtk-3.0/
cat << EOF > gtk.css
/* lockscreen background */
phosh-lockscreen, .phosh-lockshield {
  background-image: url('file://$HOME/.themes/fasttheme/tuxgnu.png');
  background-size: cover;
  background-position: top;
}

/* applist background */
phosh-app-grid {
  background-image: url('file://$HOME/.themes/fasttheme/tuxgnu.png');
  background-size: cover;
  background-position: top;
}
/* quicksettings background */
phosh-top-panel {
  background-image: url('file://$HOME/.themes/fasttheme/tuxround.png');
  background-size: cover;
  background-position: top;
}

/* background of some areas what are visible on top and bottom */
phosh-home {
  background-image: url('file://$HOME/.themes/fasttheme/tuxgnu.png');
  background-size: cover;
  background-position: top;
}

/* background of running apps list */
.phosh-overview { 
  background-image: url('file://$HOME/.themes/fasttheme/tuxgnu.png');
  background-size: cover;
  background-position: top;
}

/* squeekboard background */
sq_view {
    background: #00324d;
    color: #ffffff; /* color of the characters in the key */
}

/* squeekboard buttons */
sq_view sq_button {
    background: transparent;
    color: #ffffff; /* color of the characters in the key */
    margin: 0.5px;
    border-color: #ffffff;
    border: solid;
    border-width: 0.5px;
}

/* squeekboard locked buttons - for example press shift twice */
sq_button.locked {
    background: #ffffff;
    color: #00324d; /* color of the characters in the key */
}

/* squeekboard latched buttons - for example press shift once */
sq_button.latched {
    background: transparent;
    border-color: #ffffff;
    border: solid;
    border-width: 1.5px;
    margin: 0.5px;
    border-radius: 0px;
}

/* squeekboard pressed button to indicate what did you type */
sq_button:active {
    background: transparent;
    border-color: #ffffff;
    border: solid;
    border-width: 1px;
    margin: 0.5px;
    border-radius: 0px;
}

/* squeekboard my just a tweak to my own layout */
sq_button.small2 {
    font-size: 0.5em;
}

/* quicksetting button color */
#phosh_quick_settings flowboxchild > button {
  background-color: #c42529;
}

/* quicksetting button */
#phosh_quick_settings flowboxchild > button:hover {
  background-color: shade(#c42529, 1.14);
}

/* quicksetting button */
#phosh_quick_settings flowboxchild > button.phosh-qs-active:hover {
  background-color: shade(#c42529, 1.14);
  color:  @theme_selected_fg_color;
}

/* quicksetting button */
#phosh_quick_settings flowboxchild > button.phosh-qs-active {
  background-color: #c42529;
  color:  @theme_selected_fg_color;
}

/* quicksetting button */
#phosh_quick_settings flowboxchild > button:disabled {
  opacity: 0.5;
  background-color: #c42529;
}

/* quicksetting  */
.phosh-settings-list-box {
  background-color: #c42529;
}

/* search bar tweak (in home screen) */
.phosh-search-bar {
  background: transparent;
  border-color: #ffffff;
  color: #ffffff;
  border: solid;
  border-width: 1px;
  
}

/* search bar tweak (in home screen) */
.phosh-search-bar > image {
  color: #ffffff;
  opacity: 0.7;
}

/* search bar tweak (in home screen) */
.phosh-search-bar:focus {
  background: transparent;
  box-shadow: inset 0 0 0 0px;
  border-color: #ffffff;
}

/* volume, brightness etc. slider control */
scale trough {
  padding: 1px;
  border-radius: 2px;
  border: none;
  background: alpha(#c42529, 0.3);
}

/* volume, brightness etc. slider control */
scale trough > highlight {
  padding: 2px;
  border: none;
  background: #c42529;
}

/* volume, brightness etc. slider control */
scale trough slider {
  border-width: 1px;
  border-style: solid;
  border-color: shade(#c42529, 0.9);
  background: shade(#c42529, 0.9);
}

/* volume, brightness etc. slider control */
scale trough slider:hover {
  border-color: #c42529;
  background: #c42529;
}

/* volume, brightness etc. slider control */
scale trough slider:active {
  border-color: shade(#c42529, 0.9);
  background: shade(#c42529, 0.9);
}

/* theme colors for notifications */
@define-color phosh_activity_bg_color #c42529;
@define-color phosh_notification_bg_color #c42529;

/* theme colors for buttons */
@define-color phosh_button_bg_color #c42529;
@define-color phosh_button_hover_bg_color shade(#c42529, 1.05);
@define-color phosh_button_active_bg_color shade(#c42529, 1.1);
@define-color phosh_emergency_button_bg_color #c42529;
@define-color phosh_emergency_button_fg_color #ffffff;

/* remove shadow */
#top-bar, #home-bar {
  box-shadow: inset 0 0 0 0px;
}

/* remove shadow */
.phosh-quick-setting,
button {
  box-shadow: inset 0 0 0 0px;
}

/* remove line between favorites and apps */
phosh-app-grid separator {
  background: transparent;
  min-height: 0px;
  border-radius: 0px;
  margin: 0 12px;
}

/* volume up down bubble color */
#osd-bubble {
   background: #c42529;
}


EOF

notify-send Success Reboot



